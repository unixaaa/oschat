var nw = {
	win : null,
	trayicon : 'icon.png',
	tray:null
};

var gui = require('nw.gui');
console.log(gui.App.argv);

nw.login = (function() {
	return {
		init : function() {
			nw.win = gui.Window.get();
			//nw.win.show();
			//nw.login.winEventBind();
		},

		winEventBind : function() {
			
			nw.win.on('minimize', function() {
				// Hide window
				this.hide();
				// Show tray
				nw.tray = new gui.Tray({
					icon : nw.trayicon,
					title:"Oschat"
				});
				// Show window and remove tray when clicked
				nw.tray.on('click', function() {
					win.show();
					this.remove();
					nw.tray = null;
				});
			});

			nw.win.on('close',function(){
				this.hide();
				this.close(true);
			});
		}
	};
})();