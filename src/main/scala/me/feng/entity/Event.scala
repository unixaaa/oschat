package me.feng.entity

import java.io.Serializable
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

case class Event(u: User, cid: String, event: EventType.Value)

object EventType extends Enumeration {

  type EventType = Value
  val LOGIN, CLOSE, EIXT_SESSION,NEW_SESSION, REFRESH_ONLINE_USERS, LOGIN_SUCCESS, LOGIN_FAILE = Value
}

