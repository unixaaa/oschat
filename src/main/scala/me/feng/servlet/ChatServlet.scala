package me.feng.servlet

import javax.servlet.annotation.WebServlet
import org.apache.catalina.websocket.WebSocketServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.apache.catalina.websocket.StreamInbound
import akka.actor.ActorSystem
import me.feng.messageinbound.MainChart
import java.util.logging.Logger
import akka.actor.ActorRef
import akka.actor.actorRef2Scala


@WebServlet(Array("/chat.ws"))
class ChatServlet extends WebSocketServlet {
	
	// Log
  private val logger = Logger.getLogger(classOf[ChatServlet].getName());
  
  var cid:String = _
  var uid:String = _
  

  override def doGet(req: HttpServletRequest, resp: HttpServletResponse) {
	cid = req.getParameter("cid")
	uid = req.getParameter("uid")
    super.doGet(req, resp)
  }

  override def createWebSocketInbound(subProtocol: String, request: HttpServletRequest): StreamInbound = {
    println(uid+" && "+cid)
    //OschartContext.getMainActor.!(chart)
    val main = request.getServletContext().getAttribute("mainActor").asInstanceOf[ActorRef]
    val akka = request.getServletContext().getAttribute("akka").asInstanceOf[ActorSystem]
    val chart =  new MainChart(cid,akka,main)
    main ! (uid ->chart)
    chart
  }
}