package me.feng.util

import com.fasterxml.jackson.annotation.JsonCreator
import com.lambdaworks.jacks.JacksMapper
import me.feng.entity.Event
import me.feng.entity.EventType
import me.feng.entity.Message
import me.feng.entity.User
import com.fasterxml.jackson.annotation.JsonProperty
import me.feng.entity.Msg
import me.feng.entity.MessageType
import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist
import me.feng.entity.Letter
import me.feng.entity.LetterType

object Main {

  def main(args: Array[String]) {
//    val u = new User("11","aa")
//    val event = new Event(u,"0",EventType.LOGIN)
//    val msg = new Message[Event](MessageType.EVENT,event)
//    val list = List[Message[Event]](msg)
//	//println(JacksMapper.writeValueAsString[List[Message[Event]]](list))
//	
	val s = """{"t":0,"id":"0","m":"\nwww"}"""
	val n = JacksMapper.readValue[Msg](s)
	println(n.msg)
	val l  = new Letter("1", null, "1", Jsoup.clean(n.msg,Whitelist.relaxed()), LetterType.MAIN)
	println(JacksMapper.writeValueAsString[Letter](l))
  }
}

